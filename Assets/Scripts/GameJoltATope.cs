﻿using UnityEngine;
using System.Collections;

public class GameJoltATope : MonoBehaviour {

	public static int ShownFirstLaunch= 0;
	public static int ShownFiveLaunches= 0;
	public static int ShownFiveteenLaunches= 0;
	public static int ShownFiftyLaunches= 0;
	public static int ShownHundredLaunches= 0;
	public static int ShownFiveHundredLaunches= 0;
	public static int ShownThousandLaunches=0;

	public static int ShownSecretoTitulo=0;

	public static int ShownFirstBought= 0;
	public static int ShownFiveBoughts= 0;

	public int TableID = 252817;
	public bool Scorear = false;
	// Use this for initialization
	void Start () {

		ShownFirstLaunch = PlayerPrefs.GetInt ("FirstLaunch");
		ShownFiveLaunches= PlayerPrefs.GetInt ("FiveLaunches");
		ShownFiveteenLaunches = PlayerPrefs.GetInt ("FiveteenLaunches");
		ShownFiftyLaunches = PlayerPrefs.GetInt ("FiftyLaunches");
		ShownHundredLaunches = PlayerPrefs.GetInt ("HundredLaunches");
		ShownFiveHundredLaunches = PlayerPrefs.GetInt ("FiveHundredLaunches");
		ShownThousandLaunches = PlayerPrefs.GetInt ("ThousandLaunches");

		ShownSecretoTitulo = PlayerPrefs.GetInt ("SecretoTitulo");

		ShownFirstBought = PlayerPrefs.GetInt ("FirstBought");
		ShownFiveBoughts = PlayerPrefs.GetInt ("FiveBoughts");

	}
	
	// Update is called once per frame
	void Update () {
		PlayerPrefs.SetInt ("FirstLaunch", ShownFirstLaunch);
		PlayerPrefs.SetInt ("FiveLaunches", ShownFiveLaunches);
		PlayerPrefs.SetInt ("FiveteenLaunches", ShownFiveteenLaunches);
		PlayerPrefs.SetInt ("FiftyLaunches", ShownFiftyLaunches);
		PlayerPrefs.SetInt ("HundredLaunches", ShownHundredLaunches);
		PlayerPrefs.SetInt ("FiveHundredLaunches", ShownFiveHundredLaunches);
		PlayerPrefs.SetInt ("ThousandLaunches", ShownThousandLaunches);

		PlayerPrefs.SetInt ("SecretoTitulo", ShownSecretoTitulo);

		PlayerPrefs.SetInt ("FirstBought", ShownFirstBought);
		PlayerPrefs.SetInt ("FiveBoughts", ShownFiveBoughts);



		//FirstLaunch
		if ((pj.VecesTerminado == 1) && (ShownFirstLaunch == 0)) {
			GameJolt.API.Trophies.Unlock (75889);
			ShownFirstLaunch = 1;
		}
		//FiveLaunches
		if ((pj.VecesTerminado == 5) && (ShownFiveLaunches == 0)) {
			GameJolt.API.Trophies.Unlock (75914);
			ShownFiveLaunches = 1;
		}
		//FiveteenLaunches
		if ((pj.VecesTerminado == 15) && (ShownFiveteenLaunches == 0)) {
			GameJolt.API.Trophies.Unlock (75915);
			ShownFiveteenLaunches = 1;
		}
		//FiveteenLaunches
		if ((pj.VecesTerminado == 50) && (ShownFiftyLaunches == 0)) {
			GameJolt.API.Trophies.Unlock (75916);
			ShownFiftyLaunches = 1;
		}
		//FiveteenLaunches
		if ((pj.VecesTerminado == 100) && (ShownHundredLaunches == 0)) {
			GameJolt.API.Trophies.Unlock (75917);
			ShownHundredLaunches = 1;
		}
		//FiveteenLaunches
		if ((pj.VecesTerminado == 500) && (ShownFiveHundredLaunches == 0)) {
			GameJolt.API.Trophies.Unlock (75919);
			ShownFiveHundredLaunches = 1;
		}
		//ThousandLaunches
		if ((pj.VecesTerminado == 1000) && (ShownThousandLaunches == 0)) {
			GameJolt.API.Trophies.Unlock (75918);
			ShownThousandLaunches = 1;
		}

		//SecretoTitulo
		if ((Cosas_M.Secreto_Trofeo_titulo) && (ShownSecretoTitulo == 0)) {
			GameJolt.API.Trophies.Unlock (76002);
			ShownSecretoTitulo = 1;
		}


		//FirstBought
		if ((Tienda.Cant_Comprada == 1) && (ShownFirstBought == 0)) {
			GameJolt.API.Trophies.Unlock (75890);
			ShownFirstBought = 1;
		}
		//FirstBought
		if ((Tienda.Cant_Comprada == 5) && (ShownFiveBoughts == 0)) {
			GameJolt.API.Trophies.Unlock (76051);
			ShownFiveBoughts = 1;
		}

		//Score
		if (!Scorear) {
		GameJolt.API.Scores.Add (pj.HighScore, pj.Score, TableID);
			Scorear = !Scorear;
		}
	}

	public void ScoreBoards(){

		GameJolt.UI.Manager.Instance.ShowLeaderboards();

	}
	public void ShowTrophies(){

		GameJolt.UI.Manager.Instance.ShowTrophies();

	}
}
