using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System;

public class Tienda : MonoBehaviour {
	

	public Text dinero_txt;
	public GameObject Menu_menu;
	public GameObject tienda_menu;
	public GameObject Opciones_menu;
	public int multiplicador;
	public int Obj_Tienda = 0;
	public GameObject[] Objetos;
	public int[] Objetos_precio;
	public Text[] Objetos_texto;
	public Text[] Letreros;
	public static bool[] Comprables;
	public static int Cant_Comprada;
	public static int Tienda_Menu = 0;
	public static int pedo_cantidad=1;
	public static int Lanzador_color=0;
	public static int Potencia_Lanzador;
	public static bool ResetTienda = false;


	// Use this for initialization
	void Start () {
		Comprables = new bool[6];
		multiplicador = 2;
		Potencia_Lanzador = PlayerPrefs.GetInt ("Potencia_Lanzador");
		Objetos_precio [0] = PlayerPrefs.GetInt ("Precio0");
		Objetos_precio [1] = PlayerPrefs.GetInt ("Precio1");
		Objetos_precio [2] = PlayerPrefs.GetInt ("Precio2");
		Objetos_precio [3] = PlayerPrefs.GetInt ("Precio3");
		Objetos_precio [4] = PlayerPrefs.GetInt ("Precio4");
		Objetos_precio [5] = PlayerPrefs.GetInt ("Precio5");
		Cant_Comprada = PlayerPrefs.GetInt ("Compras");
		Comprables [0] = Convert.ToBoolean (PlayerPrefs.GetInt ("Comprable0"));
		Comprables [1] = Convert.ToBoolean (PlayerPrefs.GetInt ("Comprable1"));
		Comprables [2] = Convert.ToBoolean (PlayerPrefs.GetInt ("Comprable2"));
		Comprables [3] = Convert.ToBoolean (PlayerPrefs.GetInt ("Comprable3"));
		Comprables [4] = Convert.ToBoolean (PlayerPrefs.GetInt ("Comprable4"));
		Comprables [5] = Convert.ToBoolean (PlayerPrefs.GetInt ("Comprable5"));
	}
	
	// Update is called once per frame
	void Update () {
		PlayerPrefs.SetInt ("LentesCompradas",Scroll_Bar.LentesCompradas);
		PlayerPrefs.SetInt ("Dinero", Cosas_J.dinero);
		PlayerPrefs.SetInt ("Potencia_Lanzador", Potencia_Lanzador);
		PlayerPrefs.SetInt ("Cantidad de pedos", pj.pedo_Cantidad);
		PlayerPrefs.SetFloat ("Potencia_Tiempo", Cosas_J.Potencia_Tiempo);
		PlayerPrefs.SetFloat ("Pedo_Plus_x", pj.pedo_plus_x);
		PlayerPrefs.SetFloat ("Pedo_Plus_y", pj.pedo_plus_y);
		PlayerPrefs.SetFloat ("viento", pj.viento);
		PlayerPrefs.SetInt ("Precio0", Objetos_precio [0]);
		PlayerPrefs.SetInt ("Precio1", Objetos_precio [1]);
		PlayerPrefs.SetInt ("Precio2", Objetos_precio [2]);
		PlayerPrefs.SetInt ("Precio3", Objetos_precio [3]);
		PlayerPrefs.SetInt ("Precio4", Objetos_precio [4]);
		PlayerPrefs.SetInt ("Precio5", Objetos_precio [5]);
		PlayerPrefs.SetInt ("Compras", Cant_Comprada);
		PlayerPrefs.SetInt ("Comprable0", Convert.ToInt32(Comprables[0]));
		PlayerPrefs.SetInt ("Comprable1", Convert.ToInt32(Comprables[1]));
		PlayerPrefs.SetInt ("Comprable2", Convert.ToInt32(Comprables[2]));
		PlayerPrefs.SetInt ("Comprable3", Convert.ToInt32(Comprables[3]));
		PlayerPrefs.SetInt ("Comprable4", Convert.ToInt32(Comprables[4]));
		PlayerPrefs.SetInt ("Comprable5", Convert.ToInt32(Comprables[5]));

		if(Tienda_Menu == 0){
			Menu_menu.SetActive(true);
			tienda_menu.SetActive(false);
			Opciones_menu.SetActive(false);

		}
		if (Tienda_Menu == 1) {

			Menu_menu.SetActive (false);
			tienda_menu.SetActive (true);
			Opciones_menu.SetActive (false);
			if (Cosas_M.Idioma == 0) {
				dinero_txt.text = "Dinero : " + Cosas_J.dinero;
			}
			if (Cosas_M.Idioma == 1) {
				dinero_txt.text = "Money : " + Cosas_J.dinero;
			}
		}
		if (Tienda_Menu == 2) {

			Menu_menu.SetActive(false);
			tienda_menu.SetActive(false);
			Opciones_menu.SetActive(true);

		}
		if (ResetTienda) {
			Objetos_precio [0] = 250;
			Objetos_precio [1] = 1000;
			Objetos_precio [2] = 5000;
			Objetos_precio [3] = 500;
			Objetos_precio [4] = 5000;
			Objetos_precio [5] = 7500;
			ResetTienda = false;
		}

		if (Objetos_precio [0] == 0) {
			Objetos_precio [0] = 250;
		}
		if (Objetos_precio [1] == 0) {
			Objetos_precio [1] = 1000;
		}
		if (Objetos_precio [2] == 0) {
			Objetos_precio [2] = 5000;
		}
		if (Objetos_precio [3] == 0) {
			Objetos_precio [3] = 500;
		}
		if (Objetos_precio [4] == 0) {
			Objetos_precio [4] = 5000;
		}
		if (Objetos_precio [5] == 0) {
			Objetos_precio [5] = 7500;
		}


		if (Cosas_M.Idioma == 0) {
			Objetos_texto [0].text = "Vale: " + Objetos_precio [0];
			Objetos_texto [1].text = "Vale: " + Objetos_precio [1];
			Objetos_texto [2].text = "Vale: " + Objetos_precio [2];
			Objetos_texto [3].text = "Vale: " + Objetos_precio [3];
			Objetos_texto [4].text = "Vale: " + Objetos_precio [4];
			Objetos_texto [5].text = "Vale: " + Objetos_precio [5];

			Letreros[0].text = "Aumenta la cantidad de \n\npedos que te puedes tirar.";
			Letreros [1].text = "Aumenta la fuerza en el \n\nlanzamiento.";
			Letreros [2].text = "Reduce la velocidad en el \n\nlanzamiento.";
			Letreros [3].text = "Compra fabada para más \n\npotencia anal.";
			Letreros [4].text = "Ahora el viento... se paga.";
			Letreros [5].text = "Compra unos prismaticos,\nte permiten ver mas lejos\n\n¡Son ajustables!.";

		}
		if (Cosas_M.Idioma == 1) {
			Objetos_texto [0].text = "It cost: " + Objetos_precio [0];
			Objetos_texto [1].text = "It cost: " + Objetos_precio [1];
			Objetos_texto [2].text = "It cost: " + Objetos_precio [2];
			Objetos_texto [3].text = "It cost: " + Objetos_precio [3];
			Objetos_texto [4].text = "It cost: " + Objetos_precio [4];
			Objetos_texto [5].text = "It cost: " + Objetos_precio [5];

			Letreros [0] .text = "Increases the amount of \n\nfarts you can throw away.";
			Letreros [1] .text = "Increase the force in the \n \nslingshot.";
			Letreros [2] .text = "Reduce speed on \n \nthe launching.";
			Letreros [3] .text = "Your farts are more \n\n powerfull.";
			Letreros [4] .text = "Now the wind ... is paid.";
			Letreros [5] .text = "Buy a prismatics, \n they allow you to see further \n\n They are adjustable!";
		}

		if (Obj_Tienda <= -1) {

			Obj_Tienda = 5;

		}

		if (Obj_Tienda >= 6) {

			Obj_Tienda = 0;

		}

		if (Obj_Tienda == 0) {

			Objetos [0].SetActive (true);
			Objetos [1].SetActive (false);
			Objetos [2].SetActive (false);
			Objetos [3].SetActive (false);
			Objetos [4].SetActive (false);
			Objetos [5].SetActive (false);

		}

		if (Obj_Tienda == 1) {

			Objetos [0].SetActive (false);
			Objetos [1].SetActive (true);
			Objetos [2].SetActive (false);
			Objetos [3].SetActive (false);
			Objetos [4].SetActive (false);
			Objetos [5].SetActive (false);

		}

		if (Obj_Tienda == 2) {

			Objetos [0].SetActive (false);
			Objetos [1].SetActive (false);
			Objetos [2].SetActive (true);
			Objetos [3].SetActive (false);
			Objetos [4].SetActive (false);
			Objetos [5].SetActive (false);

		}

		if (Obj_Tienda == 3) {

			Objetos [0].SetActive (false);
			Objetos [1].SetActive (false);
			Objetos [2].SetActive (false);
			Objetos [3].SetActive (true);
			Objetos [4].SetActive (false);
			Objetos [5].SetActive (false);

		}

		if (Obj_Tienda == 4) {

			Objetos [0].SetActive (false);
			Objetos [1].SetActive (false);
			Objetos [2].SetActive (false);
			Objetos [3].SetActive (false);
			Objetos [4].SetActive (true);
			Objetos [5].SetActive (false);

		}
		if (Obj_Tienda == 5) {

			Objetos [0].SetActive (false);
			Objetos [1].SetActive (false);
			Objetos [2].SetActive (false);
			Objetos [3].SetActive (false);
			Objetos [4].SetActive (false);
			Objetos [5].SetActive (true);

		}

	}
	public void obj_0(){
		if ((Cosas_J.dinero >= Objetos_precio [0])&&(!Comprables[0])) {
			Cosas_J.dinero -= Objetos_precio [0];
			pj.pedo_Cantidad++;
			Objetos_precio [0] = Objetos_precio [0] * multiplicador;
			Cant_Comprada++;
		}
	}
	public void obj_1(){
		if ((Cosas_J.dinero >= Objetos_precio [1])&&(!Comprables[1])) {
			Cosas_J.dinero -= Objetos_precio [1];
			Potencia_Lanzador -= 25000;
			Objetos_precio [1] = Objetos_precio [1] * multiplicador;
			Cant_Comprada++;
		}
	}
	public void obj_2(){
		if ((Cosas_J.dinero >= Objetos_precio [2])&&(!Comprables[2])) {
			Cosas_J.dinero -= Objetos_precio [2];
			Cosas_J.Potencia_Tiempo = Cosas_J.Potencia_Tiempo * 1.075f;
			Objetos_precio [2] = Objetos_precio [2] * multiplicador;
			Cant_Comprada++;
		}
	}
	public void obj_3(){
		if ((Cosas_J.dinero >= Objetos_precio [3])&&(!Comprables[3])) {
			Cosas_J.dinero -= Objetos_precio [3];
			pj.pedo_plus_x = pj.pedo_plus_x * 1.1f;
			pj.pedo_plus_y = pj.pedo_plus_y * 1.1f;
			Objetos_precio [3] = Objetos_precio [3] * multiplicador;
			Cant_Comprada++;
		}
	}
	public void obj_4(){
		if ((Cosas_J.dinero >= Objetos_precio [4])&&(!Comprables[4])) {
			Cosas_J.dinero -= Objetos_precio [4];
			pj.viento = pj.viento + 0.35f;
			Objetos_precio [4] = Objetos_precio [4] * multiplicador;
			Cant_Comprada++;
		}
	}
	public void obj_5(){
		if ((Cosas_J.dinero >= Objetos_precio [5])&&(!Comprables[5])&&(Scroll_Bar.LentesCompradas == 0)) {
			Cosas_J.dinero -= Objetos_precio [5];
			Scroll_Bar.LentesCompradas += 1;
			Cant_Comprada++;
			Comprables [5] = true;
		}
	}



	public void SigOBJ(){

		Obj_Tienda++;

	}

	public void AntOBJ(){

		Obj_Tienda--;

	}
}
