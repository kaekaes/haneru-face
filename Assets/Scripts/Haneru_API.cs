﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class Haneru_API : MonoBehaviour {

	public static bool IsSignedIn;

	// Use this for initialization
	void Start () {
	
		GameJolt.UI.Manager.Instance.ShowSignIn ();
	}
	// Update is called once per frame
	void Update () {
	
		if (GameJolt.API.Manager.Instance.CurrentUser != null) {
			IsSignedIn = true;
		}
		if (IsSignedIn == true) {
			SceneManager.LoadScene ("Menuses");
		}
	}

	public void Menuses(){
		SceneManager.LoadScene ("Menuses");
		Tienda.Tienda_Menu = 0;
	}
}