﻿using UnityEngine;
using System.Collections;

public class EnemySuelo : MonoBehaviour {

	public float posX;
	public Rigidbody2D rb;
	public float vel;
	public int hp = 2;
	public bool grounded = false;
	public float minVel = 0.03f;
	public float maxVel = 0.45f;


	// Use this for initialization
	void Start () {
	rb = GetComponent<Rigidbody2D>();

		vel = Random.Range (minVel,maxVel);
	}

	// Update is called once per frame
	void Update () {
		posX = this.transform.position.x;

		if (hp == 0) {
		
			Destroy(this.gameObject);
		}

		if ((grounded)&&(pj.lanzado)&&(!pj.perdiste)) {

			this.transform.position =(new Vector3(this.transform.position.x+vel,this.transform.position.y, this.transform.position.z));

		}

		Cosas_J.timerEnemyS -= 1 * Time.deltaTime;
		if (Cosas_J.timerEnemyS <= 0) {
			Cosas_J.timerEnemyS = 1;
			grounded = true;
		}

	}

	void OnTriggerEnter2D(Collider2D other){

		if (other.gameObject.tag == "Player") {
			hp--;
		}
		if (other.gameObject.tag == "Suelo") {
			grounded = true;

		}
	}

	void OnTriggerStay2D(Collider2D other){

		if (other.gameObject.tag == "Suelo") {
			grounded = true;

		}
	}
		
	void OnTriggerExit2D(Collider2D other){
	
	
		if (other.gameObject.tag == "Suelo") {
		
			grounded = false;

		}
	}
}