﻿using UnityEngine;
using System.Collections;

public class EnemyCielo : MonoBehaviour {

	public float posX;
	public float alt;
	public float contador = 2;
	public Rigidbody2D rb;
	public float vel;
	public int hp = 2;
	public static int Dist_y_forz_v;
	public int cant_alt;

	// Use this for initialization
	void Start () {
		rb = GetComponent<Rigidbody2D>();
		vel = Random.Range (0.2f, 0.65f);
		cant_alt = Random.Range (5, 30);
		Dist_y_forz_v = Random.Range (5, 45);
	}

	// Update is called once per frame
	void Update () {
		contador += 0.2f;
		posX = this.transform.position.x;
		alt = Mathf.PingPong (contador, cant_alt);

		if (hp == 0) {

			Destroy(this.gameObject);
		}

		if (pj.lanzado) {

			this.transform.position =(new Vector3(this.transform.position.x+vel,alt+Dist_y_forz_v, this.transform.position.z));

		}

	}

	void OnTriggerEnter2D(Collider2D other){

		if (other.gameObject.tag == "Player") {
			hp--;
		}

	}
}