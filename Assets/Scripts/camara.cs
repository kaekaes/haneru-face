﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class camara : MonoBehaviour {

	public Transform player;
	public Vector3 offset;
	public float y;
	public float zoomSpeed = 1;
	public float targetOrtho;
	public float smoothSpeed = 2.0f;
	public float minOrtho = 1.0f;
	public float maxOrtho = 20.0f;

	void Start(){
		targetOrtho = Camera.main.orthographicSize;

	}
	void Update () {
		y = this.transform.position.y;
		transform.position = new Vector3 (player.position.x+1 + offset.x, player.position.y + 2, player.position.z - 10);
		if (y <= -1) {
		
			y = 0;
		}

		float scroll = Input.GetAxis ("Mouse ScrollWheel");
		if (scroll != 0.0f) {
			targetOrtho -= scroll * zoomSpeed;
			targetOrtho = Mathf.Clamp (targetOrtho, minOrtho, maxOrtho);
		}

		Camera.main.orthographicSize = Mathf.MoveTowards (Camera.main.orthographicSize, targetOrtho, smoothSpeed * Time.deltaTime);
	
	


	}
}
